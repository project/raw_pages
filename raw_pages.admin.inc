<?php
/**
 * @file
 * Admin functionality for the Raw Page module.
 */

/**
 * Menu callback for editing raw pages.
 */
function raw_pages_admin_form($form, &$form_state) {
  $form['instructions'] = array(
    '#markup' => t("<p>This allows you to create custom pages that <em>only</em> contain the text filled in below, no additional HTML is wrapped around it. The main reason to do this is to avoid cluttering the site's directory structure with tiny files.</p><p>Example uses include:</p><ul><li>Custom iframes necessary for marketing purposes.</li><li>Search engine validation.</li></ul><p>Please be careful.</p>"),
  );

  // Build a fieldset for each page.
  $ctr = 1;
  while ($path = variable_get('raw_pages_' . $ctr . '_path')) {
    $form['page_' . $ctr] = array(
      '#type' => 'fieldset',
      '#title' => 'Page ' . $ctr . ': ' . $path,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => 'To remove a page just blank out the "path" value below.',
    );
    $form['page_' . $ctr]['raw_pages_' . $ctr . '_path'] = array(
      '#type' => 'textfield',
      '#title' => 'Path',
      '#description' => 'The full path for this page. It\'s recommended to only use lowercase letters, numbers, dashes, the underline character and period, but there is no limitation on what is actually used. Examples include "my_iframe.html", "validatemysiteplease.xml", "will/this/work.html". Leaving this blank will remove the page.',
      '#default_value' => $path,
    );
    $form['page_' . $ctr]['raw_pages_' . $ctr . '_content'] = array(
      '#type' => 'textarea',
      '#rows' => 5,
      '#cols' => 60,
      '#title' => 'Content',
      '#description' => 'This content will be output exactly as-is, no additional HTML will be added. <em>No</em> validation is performed on this, so be careful.',
      '#default_value' => variable_get('raw_pages_' . $ctr . '_content'),
    );
    // Try the next one.
    $ctr++;
  }

  $form['page_' . $ctr] = array(
    '#type' => 'fieldset',
    '#title' => 'Add a new page',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => 'Fill in values below to create a new page.',
  );
  $form['page_' . $ctr]['raw_pages_' . $ctr . '_path'] = array(
    '#type' => 'textfield',
    '#title' => 'Path',
    '#description' => 'The full path for this page. It\'s recommended to only use lowercase letters, numbers, dashes, the underline character and period, but there is no limitation on what is actually used. Examples include "my_iframe.html", "validatemysiteplease.xml", "will/this/work.html". Leaving this blank will remove the page.',
  );
  $form['page_' . $ctr]['raw_pages_' . $ctr . '_content'] = array(
    '#type' => 'textarea',
    '#title' => 'Content',
    '#rows' => 5,
    '#cols' => 60,
    '#description' => 'This content will be output exactly as-is, no additional HTML will be added. <em>No</em> validation is performed on this, so be careful.',
    '#default_value' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/terms/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
  <head profile="http://www.w3.org/1999/xhtml/vocab">
    <title>What happens here?</title>
  </head>
  <body><p>Hello there!</p></body>
</html>',
  );

  // Track the number of menu items.
  $form['number_of_items'] = array(
    '#type' => 'value',
    '#value' => $ctr,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  return $form;
}

/**
 * Validation callback for the Raw Pages admin form.
 */
function raw_pages_admin_form_validate($form, &$form_state) {
  // for ($ctr = 1; $ctr <= $form_state['values']['number_of_items']; $ctr++) {
  // }
}

/**
 * Submission callback for the Raw Pages admin form.
 */
function raw_pages_admin_form_submit($form, &$form_state) {
  $pages = array();
  $vars_count = 0;
  for ($ctr = 1; $ctr <= $form_state['values']['number_of_items']; $ctr++) {
    if (!empty($form_state['values']['raw_pages_' . $ctr . '_path'])) {
      $pages[$form_state['values']['raw_pages_' . $ctr . '_path']] = $form_state['values']['raw_pages_' . $ctr . '_content'];
    }
    $vars_count = $ctr;
  }

  // Sort the pages.
  ksort($pages);

  // Save the page data.
  $ctr = 1;
  foreach ($pages as $path => $content) {
    variable_set('raw_pages_' . $ctr . '_path', $path);
    variable_set('raw_pages_' . $ctr . '_content', $content);
    $ctr++;
  }

  // Clean up the data.
  if ($ctr < $vars_count) {
    for ($cleanup = $ctr; $cleanup <= $vars_count; $cleanup++) {
      dpm("Deleting {$cleanup}");
      variable_del('raw_pages_' . $cleanup . '_path');
      variable_del('raw_pages_' . $cleanup . '_content');
    }
  }

  // Once everything is done, rebuild the menus.
  menu_rebuild();
}
