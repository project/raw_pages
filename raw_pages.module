<?php
/**
 * @file
 * Main hook implementations for the Raw Page module.
 */

/**
 * Implements hook_menu().
 */
function raw_pages_menu() {
  $items = array();

  $items['admin/config/content/raw-pages'] = array(
    'title' => 'Raw pages',
    'description' => 'Create and manage raw pages.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('raw_pages_admin_form'),
    'access arguments' => array('administer raw pages'),
    'file' => 'raw_pages.admin.inc',
  );

  // Load each of the raw pages.
  $page_id = 1;
  while ($path = variable_get('raw_pages_' . $page_id . '_path')) {
    // The page ID is prefixed by a string to work around how the menu system
    // handles numbers.
    $items[$path] = array(
      'title' => 'Raw page #' . $page_id,
      'description' => 'A custom raw page.',
      'page callback' => 'raw_pages_display_page',
      'page arguments' => array('rawpage:' . $page_id),
      'file' => 'raw_pages.display.inc',
      'access arguments' => array('access content'),
    );
    // Try the next one.
    $page_id++;
  }

  return $items;
}

/**
 * Implements hook_permission().
 */
function raw_pages_permission() {
  $permissions['administer raw pages'] = array(
    'title' => t('Administer raw pages'),
    'description' => t('Extreme caution should be taken with this, only give this permission to trusted users.'),
  );
  return $permissions;
}
