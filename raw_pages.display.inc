<?php
/**
 * @file
 * Display a Raw Page.
 */

/**
 * Menu callback for displaying raw pages.
 */
function raw_pages_display_page($path) {
  // Due to oddities in the menu system the $page_id had to be prefixed.
  $page_id = intval(str_replace('rawpage:', '', $path));

  // Load the content.
  $content = variable_get('raw_pages_' . $page_id . '_content');

  // If nothing was found, bail.
  if (empty($content)) {
    watchdog('raw_pages', 'Attempted to load raw page #:page_id but it was empty.', array(':page_id' => $page_id));
    drupal_not_found();
  }

  // Something was found, so display it.
  else {
    print $content;
    exit();
  }
}
