Raw Pages
---------
A quick 'n dirty module for providing a way to manually create some very basic
raw HTML pages.  This can be useful for e.g. creating a simple iframe page for
marketing purposes.

This module does not require the bad_judgement module, but it is recommended.
